package br.edu.up.cadastrodepessoas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Java que representa o banco de dados.
 */
public class BancoDeDados extends SQLiteOpenHelper {


    public BancoDeDados(Context context) {
        super(context, "exemplo", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE `pessoas` (" +
                "`id` INTEGER PRIMARY KEY AUTOINCREMENT," +
                "`nome` TEXT, `estado_civil` TEXT )";
        db.execSQL(sql);
    }

    /**
     Deve-se evitar a concatenação do SQL
     para não ocorrer injeção de SQL.

     String nome = pessoa.getNome();
     String estado = pessoa.getEstadoCivil();

     String sql = "insert into pessoas (nome, estado_civil)" +
     " values ('" + nome + "', '" + estado + "');";
     //" values ('João', 'Casado');";

     SQLiteDatabase db = getWritableDatabase();
     db.execSQL(sql);
     **/

    public void gravar(Pessoa pessoa){

        ContentValues valores = new ContentValues();
        valores.put("nome", pessoa.getNome());
        valores.put("estado_civil", pessoa.getEstadoCivil());

        //Carrega o banco SQLite;
        SQLiteDatabase db = getWritableDatabase();

        if (pessoa.getId() > 0){
            String id = String.valueOf(pessoa.getId());
            db.update("pessoas", valores, "id=?", new String[]{id});
        } else {
            db.insert("pessoas", null, valores);
        }
        db.close();
    }

    public void excluir(Pessoa pessoa){

        String id = String.valueOf(pessoa.getId());
        SQLiteDatabase db = getWritableDatabase();
        db.delete("pessoas", "id=?", new String[]{id});
        db.close();

        //String sql = "delete from pessoas where id = " +  pessoa.getId();
        //db.execSQL(sql);

    }

    public List<Pessoa> listar(){

        List<Pessoa> lista = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("pessoas", null, null, null, null,null, null);

        while (cursor.moveToNext()){

            int idx0 = cursor.getColumnIndex("id");
            int idx1 = cursor.getColumnIndex("nome");
            int idx2 = cursor.getColumnIndex("estado_civil");

            Pessoa pessoa = new Pessoa();
            pessoa.setId(cursor.getInt(idx0));
            pessoa.setNome(cursor.getString(idx1));
            pessoa.setEstadoCivil(cursor.getString(idx2));

            lista.add(pessoa);
        }
        db.close();
        return lista;
    }

    public Pessoa buscar(int id){

        Pessoa pessoa = null;

        String str = String.valueOf(id);
        SQLiteDatabase db = getReadableDatabase();
        //select * from pessoas where id = ?;
        Cursor cursor = db.query("pessoas",null, "id=?", new String[]{str}, null,null,null);

        if (cursor.moveToNext()){

            int idx0 = cursor.getColumnIndex("id");
            int idx1 = cursor.getColumnIndex("nome");
            int idx2 = cursor.getColumnIndex("estado_civil");

            pessoa = new Pessoa();
            pessoa.setId(cursor.getInt(idx0));
            pessoa.setNome(cursor.getString(idx1));
            pessoa.setEstadoCivil(cursor.getString(idx2));
        }
        db.close();
        return pessoa;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       //Necessário quando for atualizar o banco.
    }
}
