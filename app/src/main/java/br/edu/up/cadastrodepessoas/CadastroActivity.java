package br.edu.up.cadastrodepessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity {

    Pessoa pessoa;
    EditText txtNome;
    EditText txtEstado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        Intent intent = getIntent();
        pessoa = (Pessoa) intent.getSerializableExtra("pessoa");

        txtNome = (EditText)findViewById(R.id.txtNome);
        txtEstado = (EditText) findViewById(R.id.txtEstado);

        if (pessoa == null){
            pessoa = new Pessoa();
        } else {
            txtNome.setText(pessoa.getNome());
            txtEstado.setText(pessoa.getEstadoCivil());
        }
    }

    public void onClickGravar(View v){

        pessoa.setNome(txtNome.getText().toString());
        pessoa.setEstadoCivil(txtEstado.getText().toString());

        BancoDeDados banco = new BancoDeDados(this);
        banco.gravar(pessoa);

        finish();
    }

}
