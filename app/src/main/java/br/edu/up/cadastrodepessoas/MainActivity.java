package br.edu.up.cadastrodepessoas;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<Pessoa> adapter;
    List<Pessoa> lista;
    BancoDeDados banco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        banco = new BancoDeDados(this);
        lista = banco.listar();

        int layout = android.R.layout.simple_list_item_1;
        adapter = new ArrayAdapter<>(this, layout, lista);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa pessoa = adapter.getItem(position);

                int idx = pessoa.getId();
                Pessoa p = banco.buscar(idx);


                Intent intent = new Intent(MainActivity.this, CadastroActivity.class);
                intent.putExtra("pessoa", pessoa);
                startActivityForResult(intent, 0);
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final Pessoa pessoa = adapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("ATENÇÃO");
                builder.setMessage("Tem certeza que seja excluir?");
                builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        banco.excluir(pessoa);
                        lista.clear();
                        lista.addAll(banco.listar());
                        adapter.notifyDataSetChanged();
                    }
                });

                builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //não faz nada...
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });



    }

    public void onClickCadastrar(View v){

        Intent intent = new Intent(this, CadastroActivity.class);
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        lista.clear();
        lista.addAll(banco.listar());
        adapter.notifyDataSetChanged();
    }
}
